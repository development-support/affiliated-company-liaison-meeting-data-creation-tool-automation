Attribute VB_Name = "Module_Common"
' 参照設定
'
' Microsoft ActiveX Data Objects 6.1 Library
' Microsoft Scripting Runtime
' Windows Script Host Object Model
'
Option Explicit

'*** Setting Data ***
' Home sheet name
Public Const gHomeSheet As String = "Setting"

Type tSettingInfo
  Year As String          ' 年度
  Quarter As String       ' 四半期
  CurrentData As String   ' 今期データのシート名
  PreviousData As String  ' 前期データのシート名
  OldS3Data As String     ' 前期のスライド３用シート名
  OldS4Data As String     ' 前期のスライド４用シート名
End Type

' 設定値データリスト
Public gSettingList As tSettingInfo

' 関係会社名リスト
Public gOfficeList() As String

' 部署名リスト
Public gJobInfoList() As String

' 関係会社の関連情報型(スライド３用)
Type tOfficeInfoS3
  OfficeName As String  ' 関係会社名
  RngOffice As Range    ' 関係会社名が記載されているセル
  RngPaidFree As Range  ' 有償/無償が記載されているセル
  RngPaid As Range      ' 有償が記載されているセル
  RngFree As Range      ' 無償が記載されているセル
End Type

' 関係会社の関連情報リスト(スライド３用)
Public gOfficeInfoS3() As tOfficeInfoS3

' 関係会社の関連情報型(スライド４用)
Type tOfficeInfoS4
  OfficeName As String       ' 関係会社名
  RngOffice As Range         ' 関係会社名が記載されているセル
  RngPaidFree As Range       ' 有償/無償が記載されているセル
  RngPaid As Range           ' 有償が記載されているセル
  RngFree As Range           ' 無償が記載されているセル
  PaidFreeJobInfo() As Range ' 有償/無償カテゴリの部署名が記載されているセル
  PaidFreeTotal As Range     ' 有償/無償カテゴリの総計が記載されているセル
  PaidJobInfo() As Range     ' 有償カテゴリの部署名が記載されているセル
  PaidTotal As Range         ' 有償カテゴリの総計が記載されているセル
  FreeJobInfo() As Range     ' 無償カテゴリの部署名が記載されているセル
  FreeTotal As Range         ' 無償カテゴリの総計が記載されているセル
End Type

' 関係会社の関連情報リスト(スライド４用)
Public gOfficeInfoS4() As tOfficeInfoS4


' 指定シートが存在するかチェックするFunction
' [引数]
'   strSheetName As String：シート名
'
' [戻り値]
'   Boolean：True  (指定シートが存在する)
'          ：False (指定シートが存在しない)
Public Function isSheetExist(ByVal strSheetName As String) As Boolean
  Dim ws As Worksheet
  Dim FindFlg As Boolean

  FindFlg = False

  For Each ws In Worksheets
    If ws.Name = strSheetName Then
      FindFlg = True
      Exit For
    End If
  Next ws

  If FindFlg = False Then
    isSheetExist = False
    Exit Function
  End If

  isSheetExist = True

End Function

' 指定した名前のシートを作成するFunction
' [引数]
'   strOutSheetName As String：新規作成するシート名
'
' [戻り値]
'   なし
Public Function MakeSheet(ByVal strOutSheetName As String)
  Dim ws As Worksheet
  Dim SameSheetFlag As Boolean

  ' 同名のワークシートを再表示＆削除
  For Each ws In Worksheets
    If (UCase(ws.Name) = UCase(strOutSheetName)) Then
      SameSheetFlag = True
      Sheets(strOutSheetName).Visible = xlSheetVisible

      Application.DisplayAlerts = False
      ws.Delete
      Application.DisplayAlerts = True
    End If
  Next

  ' 指定名のワークシートを追加
  Sheets.Add After:=Worksheets(gHomeSheet)
  ActiveSheet.Name = strOutSheetName

End Function

' 「Setting」シートから設定値を取得するFunction
' [引数]
'   なし
'
' [戻り値]
'   なし
Public Function GetSetting()
  Dim wsHome As Worksheet

  ' 「(2)Create data for slide3」ボタンがあるシートをHomeに設定
  Set wsHome = Worksheets(gHomeSheet)

  gSettingList.Year = wsHome.Range("Year_Number").Value
  gSettingList.Quarter = wsHome.Range("Quarter_Number").Value
  gSettingList.CurrentData = wsHome.Range("Current_data").Value
  gSettingList.PreviousData = wsHome.Range("Previous_data").Value
  gSettingList.OldS3Data = wsHome.Range("Old_slide3_data").Value
  gSettingList.OldS4Data = wsHome.Range("Old_slide4_data").Value

  Debug.Print "(GetSetting):Year = " & gSettingList.Year & _
              " Quarter = " & gSettingList.Quarter & vbCrLf & _
              "             CurrentData = " & gSettingList.CurrentData & _
              " PreviousData = " & gSettingList.PreviousData & _
              " OldS3Data = " & gSettingList.OldS3Data & _
              " OldS4Data = " & gSettingList.OldS4Data
End Function

' 実行前のチェックを行うFunction
' [引数]
'   なし
'
' [戻り値]
'   Boolean：True  (チェックOK)
'          ：False (チェックNG)
Public Function PreExeCheck() As Boolean

  ' 各シートの存在チェック
  If (isSheetExist(gSettingList.CurrentData) = False) Then
    MsgBox "Sheet Name(Recent data)：" & gSettingList.CurrentData & vbCrLf & _
           "が存在しません。設定値を見直して下さい。"
    PreExeCheck = False
  End If

  If (isSheetExist(gSettingList.PreviousData) = False) Then
    MsgBox "Sheet Name(Previous data)：" & gSettingList.PreviousData & vbCrLf & _
           "が存在しません。設定値を見直して下さい。"
    PreExeCheck = False
  End If

  If (isSheetExist(gSettingList.OldS3Data) = False) Then
    MsgBox "SheetName(Old slide3 data)：" & gSettingList.OldS3Data & vbCrLf & _
           "が存在しません。設定値を見直して下さい。"
    PreExeCheck = False
  End If

  If (isSheetExist(gSettingList.OldS4Data) = False) Then
    MsgBox "SheetName(Old slide4 data)：" & gSettingList.OldS4Data & vbCrLf & _
           "が存在しません。設定値を見直して下さい。"
    PreExeCheck = False
  End If

  PreExeCheck = True

End Function

' 指定シートのデータに、指定したテーブル名を付けるFunction
' [引数]
'   strSheetName As String：基データがあるシート名
'   strTableName As String：指定テーブル名
'
' [戻り値]
'   Boolean：True  (処理成功)
'          ：False (処理失敗)
Public Function MakeTable(ByVal strSheetName As String, _
                          ByVal strTableName As String) As Boolean
  Dim OrgWs As Worksheet
  Dim ObjWs As Worksheet
  Dim ObjList As ListObject
  Dim i As Long

  Debug.Print "(MakeTable):strSheetName = " & strSheetName & _
              " strTableName = " & strTableName

  On Error GoTo 0
  On Error GoTo MAKE_TABLE_ERROR

  Set OrgWs = ActiveSheet
  Set ObjWs = Worksheets(strSheetName)

  ' テーブルが存在する場合、全てのテーブル設定を解除する
  If ObjWs.ListObjects.Count <> 0 Then
    For Each ObjList In ObjWs.ListObjects
      ObjList.Unlist
    Next ObjList
  End If

  ' シート切り替え後、テーブル設定
  ObjWs.Activate
  ObjWs.ListObjects.Add(xlSrcRange, Range("A1").CurrentRegion).Name = strTableName
  ' テーブルの装飾を無色にする
  Range("A1").ListObject.TableStyle = ""

  ' 元のシートに切替
  OrgWs.Activate
  MakeTable = True
  Exit Function

MAKE_TABLE_ERROR:
  MsgBox "シート(" & strSheetName & ")への" & _
         "テーブル(" & strTableName & ")設定が失敗しました。"
  MakeTable = False

End Function

' 指定文字列から、数値のみを抜き出すFunction
' [引数]
'   strVal As String：任意の文字列
'
' [戻り値]
'   String：数字のみを抜き出した文字列
Function FindNumberRegExp(ByVal strVal As String) As String
  Dim objRE

  Set objRE = CreateObject("VBScript.RegExp")

  With objRE
    .Pattern = "[^0-9]"
    .Global = True
    FindNumberRegExp = .Replace(strVal, "")
  End With
End Function

' プログレスバーの処理進捗率を更新するFunction
' [引数]
'   Percent As Long：プログレスバーに表示する％値
'
' [戻り値]
'   なし
Public Function UpdateProgressBar(ByVal Percent As Long)
  With Info
    .ProgressBar1.Value = Percent
    .Percent.Caption = Int(Percent / .ProgressBar1.max * 100) & "%"
    .Repaint
  End With
End Function
