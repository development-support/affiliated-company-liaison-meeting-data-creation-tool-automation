Attribute VB_Name = "MakeS3_Sub"
Option Explicit

' 指定シートの右側[I〜O列]のデータを左側[A〜G列]に上書きコピー後、
' 旧Quarterデータを削除するFunction
' [引数]
'   strSheetName As String：データがあるシート名
'
' [戻り値]
'   なし
Public Function CopyDelDataForS3(ByVal strSheetName As String)
  Dim OrgWs As Worksheet
  Dim ObjWs As Worksheet

  Debug.Print "(CopyDelDataForS3):strSheetName = " & strSheetName

  Set OrgWs = ActiveSheet
  Set ObjWs = Worksheets(strSheetName)

  ' シート切り替え
  ObjWs.Activate
  ' 右側[I〜O列]のデータを左側[A〜G列]にコピーする
  Range("I:O").Copy Destination:=Range("A:G")
  ' I〜O列までを削除する
  Range("I:O").Delete

  ' 元のシートに切替
  OrgWs.Activate

End Function

' 指定シートのデータを基にPivot Tableを作成するFunction
' [引数]
'   strSheetName As String：Pivot Tableを出力するシート名
'   strTableName As String：Pivot Tableの作成基となるテーブル名
'
' [戻り値]
'   Boolean：True  (処理成功)
'          ：False (処理失敗)
Public Function MakeS3PivotTable(ByVal strSheetName As String, _
                                 ByVal strTableName As String) As Boolean

  Debug.Print "(MakeS3PivotTable):strSheetName = " & strSheetName & _
              " strTableName = " & strTableName

  On Error GoTo 0
  On Error GoTo MAKE_S3_PIVOT_TABLE_ERROR

  ' ピボットテーブル作成
  ActiveWorkbook.PivotCaches.Create(xlDatabase, _
    strTableName).CreatePivotTable Sheets.Add.Range("A4")

  ' シート名を指定のものに変更
  ActiveSheet.Name = strSheetName

  ' フィールドのレイアウトを設定
  With ActiveSheet.PivotTables(1)
    ' 列フィールド
    .PivotFields("job_informal_jp").Orientation = xlRowField
    .PivotFields("order_name").Orientation = xlRowField
    ' 行フィールド
    .PivotFields("m_index").Orientation = xlColumnField
    ' データ(Σ値)フィールド
    .PivotFields("(百万)").Orientation = xlDataField
    ' フィルターフィールド
    .PivotFields("office_name").Orientation = xlPageField
    .PivotFields("technology_division_name").Orientation = xlPageField
  End With

  ' フィールド全体を折りたたみ
  ActiveSheet.PivotTables(1).PivotFields("job_informal_jp").ShowDetail = False

  MakeS3PivotTable = True
  Exit Function

MAKE_S3_PIVOT_TABLE_ERROR:
  MsgBox "テーブル(" & strTableName & _
         ")を基にしたピボットテーブル作成が失敗しました。" & vbCrLf & _
         "Err.Source = " & Err.Source & vbCrLf & _
         "Err.Number = " & Err.Number & vbCrLf & _
         "Err.Description = " & Err.Description
  MakeS3PivotTable = False

End Function

' スライド３用出力シートにおいて、今期Quarterデータのテンプレートを
' 作成するFunction
' [引数]
'   strS3Sheet As String   ：スライド３用出力シート名
'   strPivotSheet As String：スライド３用Pivot Tableを作成したシート名
'
' [戻り値]
'   なし
Public Function MakeS3Template(ByVal strS3Sheet As String, _
                               ByVal strPivotSheet As String)
  Dim OrgWs As Worksheet
  Dim S3Ws As Worksheet
  Dim PivotWs As Worksheet
  Dim maxOfficeNum As Long
  Dim maxJobInfoNum As Long
  Dim i As Long
  Dim j As Long
  Dim k As Long
  Dim rowCur As Long
  Dim colCur As Long

  Debug.Print "(MakeS3Template):strS3Sheet = " & strS3Sheet & _
              " strPivotSheet = " & strPivotSheet

  Set OrgWs = ActiveSheet
  Set S3Ws = Worksheets(strS3Sheet)
  Set PivotWs = Worksheets(strPivotSheet)

  ' Pivotテーブルのシートに切替
  PivotWs.Activate

  ' Pivotテーブルより必要情報を収集
  With ActiveSheet.PivotTables(1)
    ' Pivotテーブルより、全関係会社名(office_name)を取得
    maxOfficeNum = .PivotFields("office_name").PivotItems.Count
    ReDim gOfficeList(maxOfficeNum)
    ReDim gOfficeInfoS3(maxOfficeNum)

    For i = 1 To maxOfficeNum
      gOfficeList(i) = .PivotFields("office_name").PivotItems(i)
      gOfficeInfoS3(i).OfficeName = .PivotFields("office_name").PivotItems(i)
      Debug.Print "gOfficeList(" & i & ") = " & gOfficeList(i) & vbCrLf
    Next i

    ' Pivotテーブルより、全部署名(job_informal_jp)を取得
    maxJobInfoNum = .PivotFields("job_informal_jp").PivotItems.Count
    ReDim gJobInfoList(maxJobInfoNum)

    For i = 1 To maxJobInfoNum
      gJobInfoList(i) = .PivotFields("job_informal_jp").PivotItems(i)
      Debug.Print "gJobInfoList(" & i & ") = " & gJobInfoList(i) & vbCrLf
    Next i
  End With

  ' スライド３用出力シートに切替
  S3Ws.Activate

  ' "I1"セルを今期Quarterに書き換える
  S3Ws.Range("I1").Value = gSettingList.Quarter & "QRF"
  rowCur = S3Ws.Range("I1").Row + 2
  colCur = S3Ws.Range("I1").Column

  For i = 1 To maxOfficeNum
    ' 関係会社名を記載
    S3Ws.Cells(rowCur, colCur).Value = gOfficeInfoS3(i).OfficeName
    Set gOfficeInfoS3(i).RngOffice = _
            S3Ws.Range(S3Ws.Cells(rowCur, colCur).Address(False, False))
    rowCur = rowCur + 1
    colCur = colCur + 1
    ' 有償/無償のひな形をTempleteシートからコピー
    Range("S3_有償_無償").Copy Destination:=S3Ws.Cells(rowCur, colCur)
    Set gOfficeInfoS3(i).RngPaidFree = _
            S3Ws.Range(S3Ws.Cells(rowCur, colCur).Address(False, False))
    rowCur = rowCur + 2
    ' 全部署名を記載
    For j = 1 To maxJobInfoNum
      S3Ws.Cells(rowCur, colCur).Value = gJobInfoList(j)

      For k = 1 To 5
        Select Case (k)
          Case 5
            ' SUM関数を設定
            S3Ws.Cells(rowCur, colCur + k).Value = "=SUM(" & _
              S3Ws.Cells(rowCur, colCur + 1).Address(False, False) & _
              ":" & _
              S3Ws.Cells(rowCur, colCur + 4).Address(False, False) & ")"
          Case Else
            ' デフォルト値 = 0を設定
            S3Ws.Cells(rowCur, colCur + k).Value = 0
        End Select
      Next k

      rowCur = rowCur + 1
    Next j
    ' 総計のひな形をTempleteシートからコピー
    Range("S3_総計").Copy Destination:=S3Ws.Cells(rowCur, colCur)
    For k = 1 To 5
      ' SUM関数を設定
      S3Ws.Cells(rowCur, colCur + k).Value = "=SUM(" & _
            S3Ws.Cells(rowCur - maxJobInfoNum, colCur + k).Address(False, False) & _
            ":" & _
            S3Ws.Cells(rowCur - 1, colCur + k).Address(False, False) & ")"
    Next k

    rowCur = rowCur + 2

    ' 有償のひな形をTempleteシートからコピー
    Range("S3_有償").Copy Destination:=S3Ws.Cells(rowCur, colCur)
    Set gOfficeInfoS3(i).RngPaid = _
            S3Ws.Range(S3Ws.Cells(rowCur, colCur).Address(False, False))
    rowCur = rowCur + 2
    ' 全部署名を記載
    For j = 1 To maxJobInfoNum
      S3Ws.Cells(rowCur, colCur).Value = gJobInfoList(j)

      For k = 1 To 5
        Select Case (k)
          Case 5
            ' SUM関数を設定
            S3Ws.Cells(rowCur, colCur + k).Value = "=SUM(" & _
              S3Ws.Cells(rowCur, colCur + 1).Address(False, False) & _
              ":" & _
              S3Ws.Cells(rowCur, colCur + 4).Address(False, False) & ")"
          Case Else
            ' デフォルト値 = 0を設定
            S3Ws.Cells(rowCur, colCur + k).Value = 0
        End Select
      Next k

      rowCur = rowCur + 1
    Next j
    ' 総計のひな形をTempleteシートからコピー
    Range("S3_総計").Copy Destination:=S3Ws.Cells(rowCur, colCur)
    For k = 1 To 5
      ' SUM関数を設定
      S3Ws.Cells(rowCur, colCur + k).Value = "=SUM(" & _
            S3Ws.Cells(rowCur - maxJobInfoNum, colCur + k).Address(False, False) & _
            ":" & _
            S3Ws.Cells(rowCur - 1, colCur + k).Address(False, False) & ")"
    Next k

    rowCur = rowCur + 2

    ' 無償のひな形をTempleteシートからコピー
    Range("S3_無償").Copy Destination:=S3Ws.Cells(rowCur, colCur)
    Set gOfficeInfoS3(i).RngFree = _
            S3Ws.Range(S3Ws.Cells(rowCur, colCur).Address(False, False))
    rowCur = rowCur + 2
    ' 全部署名を記載
    For j = 1 To maxJobInfoNum
      S3Ws.Cells(rowCur, colCur).Value = gJobInfoList(j)

      For k = 1 To 5
        Select Case (k)
          Case 5
            ' SUM関数を設定
            S3Ws.Cells(rowCur, colCur + k).Value = "=SUM(" & _
              S3Ws.Cells(rowCur, colCur + 1).Address(False, False) & _
              ":" & _
              S3Ws.Cells(rowCur, colCur + 4).Address(False, False) & ")"
          Case Else
            ' デフォルト値 = 0を設定
            S3Ws.Cells(rowCur, colCur + k).Value = 0
        End Select
      Next k

      rowCur = rowCur + 1
    Next j
    ' 総計のひな形をTempleteシートからコピー
    Range("S3_総計").Copy Destination:=S3Ws.Cells(rowCur, colCur)
    For k = 1 To 5
      ' SUM関数を設定
      S3Ws.Cells(rowCur, colCur + k).Value = "=SUM(" & _
            S3Ws.Cells(rowCur - maxJobInfoNum, colCur + k).Address(False, False) & _
            ":" & _
            S3Ws.Cells(rowCur - 1, colCur + k).Address(False, False) & ")"
    Next k

    rowCur = rowCur + 2
    colCur = colCur - 1
  Next i

  ' 元のシートに切替
  OrgWs.Activate

End Function

' スライド３用Pivot Tableを操作し、各関係会社毎に３種類のデータをスライド３用出力
' シートに反映するFunction
' [引数]
'   strS3Sheet As String   ：スライド３用出力シート名
'   strPivotSheet As String：スライド３用Pivot Tableを作成したシート名
'
' [戻り値]
'   なし
Public Function CopyS3DataFromPivot(ByVal strS3Sheet As String, _
                                    ByVal strPivotSheet As String)
  Const strCostList As String = "有償/無償,有償,無償"
  Const ProgPercentOfThisProc As Long = 20  ' 本処理の全体処理に占める進捗率
  Const ProgRateUpToThisProc As Long = 80  ' 本処理までの全体進捗率
  Dim vCostList As Variant
  Dim vCostItem As Variant
  Dim OrgWs As Worksheet
  Dim S3Ws As Worksheet
  Dim PivotWs As Worksheet
  Dim i As Long
  Dim BaseProgress As Long
  Dim NowProgress As Long

  Debug.Print "(CopyS3DataFromPivot):strS3Sheet = " & strS3Sheet & _
              " strPivotSheet = " & strPivotSheet

  Set OrgWs = ActiveSheet
  Set S3Ws = Worksheets(strS3Sheet)
  Set PivotWs = Worksheets(strPivotSheet)
  vCostList = Split(strCostList, ",")
  BaseProgress = Int(ProgPercentOfThisProc / UBound(gOfficeList))

  ' Pivotテーブルのシートに切替
  PivotWs.Activate

  ' 関係会社分、繰り返し
  For i = 1 To UBound(gOfficeList)
    NowProgress = ProgRateUpToThisProc + (BaseProgress * i)
    Call UpdateProgressBar(NowProgress)

    ' 費用項目(有償/無償など)分、繰り返し
    For Each vCostItem In vCostList
      ' Pivot Tableのフィルター設定を操作
      Call OperateS3PivotTable(i, vCostItem, strPivotSheet)

      ' データが存在する部署のみ、スライド３用出力シートにコピー
      Call CopyPivotJobInfoToS3(i, vCostItem, strS3Sheet, strPivotSheet)
    Next vCostItem
  Next i

  ' 元のシートに切替
  OrgWs.Activate

End Function

' PivotTableのフィルタ設定を、指定された関係会社、費用項目に応じて
' 操作するFunction
' [引数]
'   office_idx As Long     ：関係会社(office_name)のindex番号
'   cost_itm As String     ：費用項目(有償/無償、有償、無償)
'   strPivotSheet As String：スライド３用Pivot Tableを作成したシート名
'
' [戻り値]
'   なし
Public Function OperateS3PivotTable(ByVal office_idx As Long, _
                                    ByVal cost_itm As String, _
                                    ByVal strPivotSheet As String)
  Dim OrgWs As Worksheet
  Dim PivotWs As Worksheet
  Dim itm As PivotItem

  Debug.Print "(OperateS3PivotTable):office_idx = " & office_idx & _
              " cost_itm = " & cost_itm & _
              " strPivotSheet = " & strPivotSheet

  Set OrgWs = ActiveSheet
  Set PivotWs = Worksheets(strPivotSheet)

  ' Pivotテーブルのシートに切替
  PivotWs.Activate

  With PivotWs.PivotTables(1).PivotFields("office_name")
    ' office_nameフィールドのフィルターを初期化(全て選択状態にする)
    For Each itm In .PivotItems
      itm.Visible = True
    Next itm

    ' office_nameフィールドのフィルターを設定
    For Each itm In .PivotItems
      Select Case itm.Value
        Case gOfficeInfoS3(office_idx).OfficeName
          itm.Visible = True
        Case Else
          itm.Visible = False
      End Select
    Next itm
  End With

  With PivotWs.PivotTables(1).PivotFields("technology_division_name")
    ' technology_division_nameフィールドのフィルターを設定
    Select Case cost_itm
      Case "有償/無償"
        For Each itm In .PivotItems
          itm.Visible = True
        Next itm
      Case "有償"
        For Each itm In .PivotItems
          Select Case itm.Value
            Case "フィールドサポート費", "有償ソフト(有償)"
              itm.Visible = True
            Case Else
              itm.Visible = False
          End Select
        Next itm
      Case "無償"
        For Each itm In .PivotItems
          Select Case itm.Value
            Case "技術研究費", "設計原価", "販売・技術支援費"
              itm.Visible = True
            Case Else
              itm.Visible = False
          End Select
        Next itm
    End Select
  End With

  ' 元のシートに切替
  OrgWs.Activate

End Function

' PivotTableにデータが存在する部署のみ、スライド３用出力シートに
' コピーするFunction
' [引数]
'   office_idx As Long     ：関係会社(office_name)のindex番号
'   cost_itm As String     ：費用項目(有償/無償、有償、無償)
'   strS3Sheet As String   ：スライド３用出力シート名
'   strPivotSheet As String：スライド３用Pivot Tableを作成したシート名
'
' [戻り値]
'   なし
Public Function CopyPivotJobInfoToS3(ByVal office_idx As Long, _
                                     ByVal cost_itm As String, _
                                     ByVal strS3Sheet As String, _
                                     ByVal strPivotSheet As String)
  Dim OrgWs As Worksheet
  Dim S3Ws As Worksheet
  Dim PivotWs As Worksheet
  Dim FoundCell As Range
  Dim strCopySrcRng As String
  Dim strCopyDstRng As String
  Dim i As Long

  Debug.Print "(CopyPivotJobInfoToS3):office_idx = " & office_idx & _
              " cost_itm = " & cost_itm & _
              " strS3Sheet = " & strS3Sheet & _
              " strPivotSheet = " & strPivotSheet

  Set OrgWs = ActiveSheet
  Set S3Ws = Worksheets(strS3Sheet)
  Set PivotWs = Worksheets(strPivotSheet)

  ' Pivotテーブルのシートに切替
  PivotWs.Activate

  ' データが存在する部署のみ、スライド３用出力シートにコピー
  For i = 1 To UBound(gJobInfoList)
    Set FoundCell = Cells.Find(What:=gJobInfoList(i), LookAt:=xlWhole)
    If FoundCell Is Nothing Then
      ' 部署名が見つからなかった場合、SKIP
      GoTo SKIP_JOB_INFO_S3
    End If

    ' 該当部署名の４Q分のデータをコピー
    strCopySrcRng = Cells(FoundCell.Row, FoundCell.Column + 1).Address & _
                    ":" & _
                    Cells(FoundCell.Row, FoundCell.Column + 4).Address
    Select Case (cost_itm)
      Case "有償/無償"
        strCopyDstRng = _
            S3Ws.Cells(gOfficeInfoS3(office_idx).RngPaidFree.Row + 1 + i, _
                       gOfficeInfoS3(office_idx).RngPaidFree.Column + 1).Address
      Case "有償"
        strCopyDstRng = _
            S3Ws.Cells(gOfficeInfoS3(office_idx).RngPaid.Row + 1 + i, _
                       gOfficeInfoS3(office_idx).RngPaid.Column + 1).Address
      Case "無償"
        strCopyDstRng = _
            S3Ws.Cells(gOfficeInfoS3(office_idx).RngFree.Row + 1 + i, _
                       gOfficeInfoS3(office_idx).RngFree.Column + 1).Address
    End Select

    Range(strCopySrcRng).Copy Destination:=S3Ws.Range(strCopyDstRng)
SKIP_JOB_INFO_S3:
  Next i

  ' 元のシートに切替
  OrgWs.Activate

End Function
