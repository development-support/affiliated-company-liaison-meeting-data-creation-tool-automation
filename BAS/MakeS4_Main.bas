Attribute VB_Name = "MakeS4_Main"
' 参照設定
'
' Microsoft ActiveX Data Objects 6.1 Library
' Microsoft Scripting Runtime
' Windows Script Host Object Model
'
Option Explicit

' Output sheet name for slide 4
Public gOutSlide4Name As String

' 前期/今期Quarter差分データ出力シート名
Public gS4DiffDataSheet As String


'******************************************************************************
' 「(3)Create data for slide4」ボタンをクリック
'
' [引数]
' なし
'
' [戻り値]
' なし
'
' [処理概要]
' (Pre Step)「Setting」シートに記載の設定値を取得する。
' (Step 1)実行前チェックを行う。
' (Step 2)設定値(Year＆Quarter Number)より、スライド４用出力シート名を決定する。
' (Step 3)設定値(SheetName(Old slide4 data))より、旧スライド４用シートをコピー
'         して、(Step 2)で決定したシート名を付与する。
' (Step 4)スライド４用出力シートにおいて、旧データを削除する。
'         (６行目〜最終行のデータをDelete)
' (Step 5)前Quarterのデータ(Previous data)、当該Quarterのデータ(Recent data)
'         を基に差分データを作成(シート出力)する。
' (Step 6)(Step 5)で作成した差分データにテーブル設定を行う。
' (Step 7)(Step 6)のテーブルを基に、ピボットテーブルを作成(シート出力)する。
' (Step 8)(Step 7)で作成したピボットテーブルを基に、各関係会社毎に３種類のデータ
'         「有償/無償」、「有償」、「無償」をスライド４用出力シートにコピーする。
'******************************************************************************
Public Sub btnCreateDataForSlide4()
  Dim strRecTableName As String
  Dim strPivotSheet As String

  ' プログレスバーFormを表示
  Info.Show vbModeless

  Application.ScreenUpdating = False

  ' (Pre Step)「Setting」シートに記載の設定値を取得する。
  Call GetSetting

  ' (Step 1)実行前チェックを行う。
  If (PreExeCheck = False) Then
    ' プログレスバーFormを閉じる
    Unload Info
    Exit Sub
  End If

  Call UpdateProgressBar(10)

  ' (Step 2)設定値(Year＆Quarter Number)より、スライド４用出力シート名を決定する。
  gOutSlide4Name = "S4_" & gSettingList.Year & "_" & gSettingList.Quarter & "Q"
  Debug.Print "(btnCreateDataForSlide4):strOutSlide4Name = " & gOutSlide4Name

  If (isSheetExist(gOutSlide4Name) = True) Then
    Application.DisplayAlerts = False
    Worksheets(gOutSlide4Name).Delete
    Application.DisplayAlerts = True
  End If

  Call UpdateProgressBar(20)

  ' (Step 3)設定値(SheetName(Old slide4 data))より、旧スライド４用シートをコピー
  '         して、(Step 2)で決定したシート名を付与する。
  Worksheets(gSettingList.OldS4Data).Copy After:=Worksheets(gHomeSheet)
  ActiveSheet.Name = gOutSlide4Name

  Call UpdateProgressBar(30)

  ' (Step 4)スライド４用出力シートにおいて、旧データを削除する。
  '         (６行目〜最終行のデータをDelete)
  Call DelDataForS4(gOutSlide4Name)

  Call UpdateProgressBar(40)

  ' (Step 5)前Quarterのデータ(Previous data)、当該Quarterのデータ(Recent data)
  '         を基に差分データを作成(シート出力)する。
  ' 差分データ出力用シート名を決定する。
  gS4DiffDataSheet = "S4_work_" & gSettingList.Year & "_" & gSettingList.Quarter & "Q"
  
  Call MakeS4DiffData(gSettingList.CurrentData, _
                      gSettingList.PreviousData, _
                      gS4DiffDataSheet)

  Call UpdateProgressBar(50)

  ' (Step 6)(Step 5)で作成した差分データにテーブル設定を行う。
  strRecTableName = gS4DiffDataSheet & "_tbl"
  If (MakeTable(gS4DiffDataSheet, strRecTableName) = False) Then
    ' プログレスバーFormを閉じる
    Unload Info
    Exit Sub
  End If

  Call UpdateProgressBar(60)

  ' (Step 7)(Step 6)のテーブルを基に、ピボットテーブルを作成(シート出力)する。
  strPivotSheet = gS4DiffDataSheet & "_Pivot"

  If (isSheetExist(strPivotSheet) = True) Then
    Application.DisplayAlerts = False
    Worksheets(strPivotSheet).Delete
    Application.DisplayAlerts = True
  End If

  If (MakeS4PivotTable(strPivotSheet, _
                       strRecTableName, _
                       gSettingList.Quarter) = False) Then
    ' プログレスバーFormを閉じる
    Unload Info
    Exit Sub
  End If

  Call UpdateProgressBar(70)

  ' (Step 8)(Step 7)で作成したピボットテーブルを基に、各関係会社毎に３種類のデータ
  '         「有償/無償」、「有償」、「無償」をスライド４用出力シートにコピーする。
  ' スライド４用出力シートにおいてテンプレートを作成する。
  Call MakeS4Template(gOutSlide4Name, strPivotSheet)

  ' ピボットテーブルを操作し、各関係会社毎に３種類のデータをスライド４用出力
  ' シートに反映する。
  Call CopyS4DataFromPivot(gOutSlide4Name, strPivotSheet)

  ' スライド４用出力シートに切替後、E:G列の表示形式を設定
  Worksheets(gOutSlide4Name).Activate
  ' 小数点以下の桁数：1, 負の数の表示形式：先頭に▲を付与
  Range("E:G").NumberFormatLocal = "0.0;""▲""0.0"

  ' Settingシートに切替
  Worksheets(gHomeSheet).Activate

  Call UpdateProgressBar(100)

  Application.ScreenUpdating = True
  ' プログレスバーFormを閉じる
  Unload Info

  MsgBox "スライド４データの出力処理完了" & vbCrLf & _
         "シート名：" & gOutSlide4Name, vbInformation
End Sub
