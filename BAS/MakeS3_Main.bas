Attribute VB_Name = "MakeS3_Main"
' 参照設定
'
' Microsoft ActiveX Data Objects 6.1 Library
' Microsoft Scripting Runtime
' Windows Script Host Object Model
'
Option Explicit

' Output sheet name for slide 3
Public gOutSlide3Name As String


'******************************************************************************
' 「(2)Create data for slide3」ボタンをクリック
'
' [引数]
' なし
'
' [戻り値]
' なし
'
' [処理概要]
' (Pre Step)「Setting」シートに記載の設定値を取得する。
' (Step 1)実行前チェックを行う。
' (Step 2)設定値(Year＆Quarter Number)より、スライド３用出力シート名を決定する。
' (Step 3)設定値(SheetName(Old slide3 data))より、旧スライド３用シートをコピー
'         して、(Step 2)で決定したシート名を付与する。
' (Step 4)スライド３用出力シートにおいて、２期前のデータを１期前のデータで上書き
'         (右側[I〜O列]のデータを左側[A〜G列]にコピー)＆旧データを削除する。
' (Step 5)直近のデータ(Recent data)にテーブル設定を行う。
' (Step 6)(Step 5)で設定したテーブルを基に、ピボットテーブルを作成(シート出力)する。
' (Step 7)(Step 6)で作成したピボットテーブルを基に、各関係会社毎に３種類のデータ
'         「有償/無償」、「有償」、「無償」をスライド３用出力シートにコピーする。
'******************************************************************************
Public Sub btnCreateDataForSlide3()
  Dim strRecTableName As String
  Dim strPivotSheet As String

  ' プログレスバーFormを表示
  Info.Show vbModeless

  Application.ScreenUpdating = False

  ' (Pre Step)「Setting」シートに記載の設定値を取得する。
  Call GetSetting

  ' (Step 1)実行前チェックを行う。
  If (PreExeCheck = False) Then
    ' プログレスバーFormを閉じる
    Unload Info
    Exit Sub
  End If

  Call UpdateProgressBar(10)

  ' (Step 2)設定値(Year＆Quarter Number)より、スライド３用出力シート名を決定する。
  gOutSlide3Name = "S3_" & gSettingList.Year & "_" & gSettingList.Quarter & "Q"
  Debug.Print "(btnCreateDataForSlide3):strOutSlide3Name = " & gOutSlide3Name

  If (isSheetExist(gOutSlide3Name) = True) Then
    Application.DisplayAlerts = False
    Worksheets(gOutSlide3Name).Delete
    Application.DisplayAlerts = True
  End If

  Call UpdateProgressBar(20)

  ' (Step 3)設定値(SheetName(Old slide3 data))より、旧スライド３用シートを
  '         コピーして、(step 2)で決定したシート名を付与する。
  Worksheets(gSettingList.OldS3Data).Copy After:=Worksheets(gHomeSheet)
  ActiveSheet.Name = gOutSlide3Name

  Call UpdateProgressBar(30)

  ' (Step 4)スライド３用出力シートにおいて、２期前のデータを１期前のデータで上書き
  '         (右側[I〜O列]のデータを左側[A〜G列]にコピー)＆旧データを削除する。
  Call CopyDelDataForS3(gOutSlide3Name)

  Call UpdateProgressBar(40)

  ' (Step 5)直近のデータ(Recent data)にテーブル設定を行う。
  strRecTableName = "_" & gSettingList.CurrentData & "_tbl"
  Debug.Print "(btnCreateDataForSlide3):strRecTableName = " & strRecTableName

  If (MakeTable(gSettingList.CurrentData, strRecTableName) = False) Then
    ' プログレスバーFormを閉じる
    Unload Info
    Exit Sub
  End If

  Call UpdateProgressBar(50)

  ' (Step 6)(Step 5)で設定したテーブルを基にピボットテーブルを作成
  '         (シート出力)する。
  strPivotSheet = gSettingList.CurrentData & "_Pivot"

  If (isSheetExist(strPivotSheet) = True) Then
    Application.DisplayAlerts = False
    Worksheets(strPivotSheet).Delete
    Application.DisplayAlerts = True
  End If

  If (MakeS3PivotTable(strPivotSheet, strRecTableName) = False) Then
    ' プログレスバーFormを閉じる
    Unload Info
    Exit Sub
  End If

  Call UpdateProgressBar(60)

  ' (Step 7)(Step 6)で作成したピボットテーブルを基に、各関係会社毎に３種類の
  '         データ「有償/無償」、「有償」、「無償」をスライド３用出力シートに
  '         コピーする。
  ' スライド３用出力シートにおいてテンプレートを作成する。
  Call MakeS3Template(gOutSlide3Name, strPivotSheet)

  Call UpdateProgressBar(70)

  ' ピボットテーブルを操作し、各関係会社毎に３種類のデータをスライド３用出力
  ' シートに反映する。
  Call CopyS3DataFromPivot(gOutSlide3Name, strPivotSheet)

  ' スライド３用出力シートに切替後、J列のみ列幅を自動調整
  Worksheets(gOutSlide3Name).Activate
  Columns(10).AutoFit

  ' Settingシートに切替
  Worksheets(gHomeSheet).Activate

  Call UpdateProgressBar(100)

  Application.ScreenUpdating = True
  ' プログレスバーFormを閉じる
  Unload Info

  MsgBox "スライド３データの出力処理完了" & vbCrLf & _
         "シート名：" & gOutSlide3Name, vbInformation

End Sub
