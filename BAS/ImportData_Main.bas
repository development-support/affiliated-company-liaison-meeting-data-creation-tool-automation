Attribute VB_Name = "ImportData_Main"
Option Explicit

'******************************************************************************
' 「(1)Import Recent data from File」ボタンをクリック
'
' [引数]
' なし
'
' [戻り値]
' なし
'
' [処理概要]
' (Pre Step)「Setting」シートに記載の設定値を取得する。
' (Step 1)インポート基ファイルを選択して開く。
' (Step 2)開いたファイルのシート総数=１であるかチェックする。
' (Step 3)開いたファイルにあるシートをコピーし、指定された名前に変更する。
' (Step 4)コピー先シートにおいて、A列を削除する。
' (Step 5)インポート基ファイルを閉じる。
'******************************************************************************
Public Sub btnImportRecentData()
  Dim strDataFile As String
  Dim strDataPath As String
  Dim OrgBook As Workbook
  Dim ImportBook As Workbook

  Application.ScreenUpdating = False

  ' (Pre Step)「Setting」シートに記載の設定値を取得する。
  Call GetSetting

  Set OrgBook = ActiveWorkbook

  ' (Step 1)インポート基ファイルを選択して開く。
  If (FileOpenWithDialog(strDataFile, strDataPath) = False) Then
    Exit Sub
  End If

  Set ImportBook = ActiveWorkbook

  ' (Step 2)開いたファイルのシート総数=１であるかチェックする。
  If (ImportBook.Worksheets.Count <> 1) Then
    MsgBox strDataFile & "に複数のシートがあります。" & vbCrLf & _
           "１シートになるように見直して下さい。"
    Exit Sub
  End If

  ' (Step 3)開いたファイルにあるシートをコピーし、指定された名前に変更する。
  ' コピー先に同名シートが有る場合、削除する。
  OrgBook.Activate
  If (isSheetExist(gSettingList.CurrentData) = True) Then
    Application.DisplayAlerts = False
    OrgBook.Worksheets(gSettingList.CurrentData).Delete
    Application.DisplayAlerts = True
  End If

  ImportBook.Sheets(1).Copy After:=OrgBook.Sheets(OrgBook.Sheets.Count)
  ActiveSheet.Name = gSettingList.CurrentData

  ' (Step 4)コピー先シートにおいて、A列を削除する。
  OrgBook.Worksheets(gSettingList.CurrentData).Columns(1).Delete
  ActiveSheet.Cells.EntireColumn.AutoFit

  ' (Step 5)インポート基ファイルを閉じる。
  Application.DisplayAlerts = False
  ImportBook.Close
  Application.DisplayAlerts = True

  Application.ScreenUpdating = True

  MsgBox "データインポート処理完了" & vbCrLf & _
         "シート名：" & gSettingList.CurrentData, vbInformation
End Sub
