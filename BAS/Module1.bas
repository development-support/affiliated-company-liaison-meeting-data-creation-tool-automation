Attribute VB_Name = "Module1"
' 参照設定
'
' Microsoft ActiveX Data Objects 6.1 Library
' Microsoft Scripting Runtime
' Windows Script Host Object Model
'
Option Explicit

' DB Access関連
Public objCon As ADODB.Connection
Public objRS As ADODB.Recordset

' シート名(Mainで使用)
Private Const strOutputSheetName As String = "スライド4_work_3Q_Pre"
Private Const strLastSheetName As String = "2019_0910実績_職制変更"
Private Const strActualSheetName As String = "2019_1119実績"

'''''' 処理手順 ''''''
Private Sub Main()

    ' 出力シート生成
    CreateSheet strOutputSheetName

    '' DB(Sheet)接続
    DBConnect
    ' 強調表示設定の取得
    GetMessageColor
    ' DB(Sheet)切断
    objectRelease

    ' ラベル付与(order_name->order_name2)
    差分に関するコメント付加

    ' 固定枠とフィルタ
    固定枠とフィルタ

    ' 書式設定
    Columns(3).NumberFormatLocal = "0"

    ' 列幅の調整
    'Sheets(strOutSheetName).Columns("A:H").EntireColumn.AutoFit
    'Sheets(strOutputSheetName).Range("A1").CurrentRegion.EntireColumn.AutoFit

End Sub


'''''' 強調表示設定関連 ''''''
'　強調表示に関する有効設定を抽出
Private Sub GetMessageColor()
    ' ,
    Dim i As Long
    Dim strSQL1 As String
    Dim strSQL2 As String
    Dim strSQL3 As String
    Dim strSQL As String
    Dim v As Variant
    
    ' 差分
    
    'strSQL1 = "SELECT IIf((旧.[(百万)] = 新.[(百万)]),'○','×') as 差分, 新.* " & _
    '            "FROM [2019_1QRF計画$] as 旧 INNER JOIN [2019_0605実績$] as 新 ON " & _
    '            "(旧.office_name = 新.office_name) AND (旧.technology_division_name = 新.technology_division_name) AND (旧.m_index = 新.m_index) AND (TRIM(旧.order_name) = TRIM(新.order_name))"
    
    'strSQL1 = "SELECT IIf((旧.[(百万)] = 新.[(百万)]),'○','×') as 差分, " & _
    '            "新.office_name, " & _
    '            "新.head_code, " & _
    '            "新.business_code, " & _
    '            "新.charge_post_code, " & _
    '            "新.charge_post_name, " & _
    '            "新.cost_no, " & _
    '            "新.order_name, " & _
    '            "新.[sum(acceptance_cost)]-旧.[sum(acceptance_cost)], " & _
    '            "新.[(百万)]-旧.[(百万)], " & _
    '            "新.m_index, " & _
    '            "新.technology_division_name, " & _
    '            "新.job_informal_jp " & _
    '            "FROM [2019_1QRF計画$] as 旧 INNER JOIN [2019_0605実績$] as 新 ON " & _
    '            "(旧.office_name = 新.office_name) AND (旧.technology_division_name = 新.technology_division_name) AND (旧.m_index = 新.m_index) AND (TRIM(旧.order_name) = TRIM(新.order_name))"
    
    strSQL1 = "SELECT IIf((旧.[(百万)] = 新.[(百万)]),'○','×') as 差分, " & _
                "新.office_name, " & _
                "新.head_code, " & _
                "新.business_code, " & _
                "Format(新.charge_post_code,'0000') as charge_post_code, " & _
                "新.charge_post_name, " & _
                "新.cost_no, " & _
                "新.order_name, " & _
                "新.technology_division_name, " & _
                "新.job_informal_jp, " & _
                "新.m_index, " & _
                "旧.[sum(acceptance_cost)] as [Last_sum(acceptance_cost)], " & _
                "旧.[(百万)] as [Last_(百万)], " & _
                "新.[sum(acceptance_cost)] as [New_sum(acceptance_cost)], " & _
                "新.[(百万)] as [New_(百万)], " & _
                "新.[sum(acceptance_cost)]-旧.[sum(acceptance_cost)] as [差分_sum(acceptance_cost)], " & _
                "新.[(百万)]-旧.[(百万)] as [差分_(百万)] " & _
                "FROM [" & strLastSheetName & "$] as 旧 INNER JOIN [" & strActualSheetName & "$] as 新 ON " & _
                "(旧.office_name = 新.office_name) AND (旧.technology_division_name = 新.technology_division_name) AND (旧.m_index = 新.m_index) AND (TRIM(旧.order_name) = TRIM(新.order_name))"
    
    ' 追加
    'strSQL2 = "SELECT IIf([旧].[office_name] Is Null,'追加','') AS 差分, 新.* " & _
    '            "FROM [2019_0605実績$] as 新 LEFT JOIN [2019_1QRF計画$] as 旧 ON " & _
    '            "(旧.office_name = 新.office_name) AND (旧.technology_division_name = 新.technology_division_name) AND (旧.m_index = 新.m_index) AND (TRIM(旧.order_name) = TRIM(新.order_name)) " & _
    '            "WHERE (((旧.office_name) Is Null))"
    'strSQL2 = "SELECT IIf([旧].[office_name] Is Null,'追加','') AS 差分, 新.* " & _
    '            "FROM [" & strActualSheetName & "$] as 新 LEFT JOIN [" & strLastSheetName & "$] as 旧 ON " & _
    '            "(旧.office_name = 新.office_name) AND (旧.technology_division_name = 新.technology_division_name) AND (旧.m_index = 新.m_index) AND (TRIM(旧.order_name) = TRIM(新.order_name)) " & _
    '            "WHERE (((旧.office_name) Is Null))"
    strSQL2 = "SELECT IIf([旧].[office_name] Is Null,'追加','') AS 差分, " & _
                "新.office_name, " & _
                "新.head_code, " & _
                "新.business_code, " & _
                "Format(新.charge_post_code,'0000') as charge_post_code, " & _
                "新.charge_post_name, " & _
                "新.cost_no, " & _
                "新.order_name, " & _
                "新.technology_division_name, " & _
                "新.job_informal_jp, " & _
                "新.m_index, " & _
                "0 as [Last_sum(acceptance_cost)], " & _
                "0 as [Last_(百万)], " & _
                "新.[sum(acceptance_cost)] as [New_sum(acceptance_cost)], " & _
                "新.[(百万)] as [New_(百万)], " & _
                "新.[sum(acceptance_cost)] as [差分_sum(acceptance_cost)], " & _
                "新.[(百万)] as [差分_(百万)] " & _
                "FROM [" & strActualSheetName & "$] as 新 LEFT JOIN [" & strLastSheetName & "$] as 旧 ON " & _
                "(旧.office_name = 新.office_name) AND (旧.technology_division_name = 新.technology_division_name) AND (旧.m_index = 新.m_index) AND (TRIM(旧.order_name) = TRIM(新.order_name)) " & _
                "WHERE (((旧.office_name) Is Null))"




    ' 削除
    'strSQL3 = "SELECT IIf([新].[office_name] Is Null,'削除','') AS 差分, 旧.* " & _
    '            "FROM [2019_1QRF計画$] as 旧 LEFT JOIN [2019_0605実績$] as 新 ON " & _
    '            "(旧.office_name = 新.office_name) AND (旧.technology_division_name = 新.technology_division_name) AND (旧.m_index = 新.m_index) AND (TRIM(旧.order_name) = TRIM(新.order_name)) " & _
    '            "WHERE (((新.office_name) Is Null))"
    'strSQL3 = "SELECT IIf([新].[office_name] Is Null,'削除','') AS 差分, " & _
    '            "旧.office_name, " & _
    '            "旧.head_code, " & _
    '            "旧.business_code, " & _
    '            "旧.charge_post_code, " & _
    '            "旧.charge_post_name, " & _
    '            "旧.cost_no, " & _
    '            "旧.order_name, " & _
    '            "-旧.[sum(acceptance_cost)], " & _
    '            "-旧.[(百万)], " & _
    '            "旧.m_index, " & _
    '            "旧.technology_division_name, " & _
    '            "旧.job_informal_jp " & _
    '            "FROM [2019_1QRF計画$] as 旧 LEFT JOIN [2019_0605実績$] as 新 ON " & _
    '            "(旧.office_name = 新.office_name) AND (旧.technology_division_name = 新.technology_division_name) AND (旧.m_index = 新.m_index) AND (TRIM(旧.order_name) = TRIM(新.order_name)) " & _
    '            "WHERE (((新.office_name) Is Null))"
    'strSQL3 = "SELECT IIf([新].[office_name] Is Null,'削除','') AS 差分, " & _
    '            "旧.office_name, " & _
    '            "旧.head_code, " & _
    '            "旧.business_code, " & _
    '            "旧.charge_post_code, " & _
    '            "旧.charge_post_name, " & _
    '            "旧.cost_no, " & _
    '            "旧.order_name, " & _
    '            "-旧.[sum(acceptance_cost)], " & _
    '            "-旧.[(百万)], " & _
    '            "旧.m_index, " & _
    '            "旧.technology_division_name, " & _
    '            "旧.job_informal_jp " & _
    '            "FROM [" & strLastSheetName & "$] as 旧 LEFT JOIN [" & strActualSheetName & "$] as 新 ON " & _
    '            "(旧.office_name = 新.office_name) AND (旧.technology_division_name = 新.technology_division_name) AND (旧.m_index = 新.m_index) AND (TRIM(旧.order_name) = TRIM(新.order_name)) " & _
    '            "WHERE (((新.office_name) Is Null))"
    strSQL3 = "SELECT IIf([新].[office_name] Is Null,'削除','') AS 差分, " & _
                "旧.office_name, " & _
                "旧.head_code, " & _
                "旧.business_code, " & _
                "Format(旧.charge_post_code,'0000') as charge_post_code, " & _
                "旧.charge_post_name, " & _
                "旧.cost_no, " & _
                "旧.order_name, " & _
                "旧.technology_division_name, " & _
                "旧.job_informal_jp, " & _
                "旧.m_index, " & _
                "旧.[sum(acceptance_cost)] as [Last_sum(acceptance_cost)], " & _
                "旧.[(百万)] as [Last_(百万)], " & _
                "0 as [New_sum(acceptance_cost)], " & _
                "0 as [New_(百万)], " & _
                "0-旧.[sum(acceptance_cost)] as [差分_sum(acceptance_cost)], " & _
                "0-旧.[(百万)] as [差分_(百万)] " & _
                "FROM [" & strLastSheetName & "$] as 旧 LEFT JOIN [" & strActualSheetName & "$] as 新 ON " & _
                "(旧.office_name = 新.office_name) AND (旧.technology_division_name = 新.technology_division_name) AND (旧.m_index = 新.m_index) AND (TRIM(旧.order_name) = TRIM(新.order_name)) " & _
                "WHERE (((新.office_name) Is Null))"


    ' 結合  = 差分リストの完成!
    strSQL = "select * from (" & strSQL1 & ") " & _
                "union select * from (" & strSQL2 & ") " & _
                "union select * from (" & strSQL3 & ") " & _
                "order by office_name, job_informal_jp,  charge_post_code, business_code, cost_no, order_name, m_index"

    Set objRS = objCon.Execute(strSQL)

    'If objMessageColorRS.BOF = True And objMessageColorRS.EOF = True Then
    '   MsgBox "有効な設定が存在しませんでした." & objMessageColorRS.RecordCount
    'End If
    
        '            "旧.office_name, " & _
    '            "旧.head_code, " & _
    '            "旧.business_code, " & _
    '            "旧.charge_post_code, " & _
    '            "旧.charge_post_name, " & _
    '            "旧.cost_no, " & _
    '            "旧.order_name, " & _
    '            "-旧.[sum(acceptance_cost)], " & _
    '            "-旧.[(百万)], " & _
    '            "旧.m_index, " & _
    '            "旧.technology_division_name, " & _
    '            "旧.job_informal_jp "
    
    ' 出力
    ' 列の見出し
    For i = 0 To objRS.Fields.Count - 1
        Sheets(strOutputSheetName).Range("A1").Offset(0, i).Value = objRS.Fields(i).Name
    Next
    ' データ
    Sheets(strOutputSheetName).Range("A2").CopyFromRecordset objRS
    

End Sub


Private Sub 差分に関するコメント付加()
        
    Dim rngCur As Range
    Dim rngColumn As Range
    Dim rng As Range

    Dim lngTargetColumn As Long
    lngTargetColumn = 8


    ' 1列追加
    Sheets(strOutputSheetName).Columns(lngTargetColumn).Insert shift:=xlToRight, CopyOrigin:=xlFormatFromLeftOrAbove
    ' 列の見出し付与
    Sheets(strOutputSheetName).Cells(1, lngTargetColumn).Value = "order_name2"

    ' 書き込み対象領域を取得
    ' 表の上端でCurrentRegion
    Set rngCur = Sheets(strOutputSheetName).Range("A1").CurrentRegion
    ' 列の見出しの範囲をxldown
    Set rngColumn = Sheets(strOutputSheetName).Columns(lngTargetColumn)
    ' 重なる領域を取得
    Set rng = Intersect(rngCur, rngColumn)
    
    ' 対象領域に対して、(追加)、(削除)のラベルを付与する数式を入力
    ' 数式を入力
    rng(2, 1).Formula = "=IF(OR(A2=""追加"",A2=""削除""),""(""&A2&"")""&I2,I2)"
    ' コピー
    rng(2, 1).Copy
    ' 貼り付け
    Set rng = rng.Offset(2, 0).Resize(rng.Rows.Count - 2, 1)
    rng.PasteSpecial Paste:=xlPasteFormulas, Operation:=xlNone, _
        SkipBlanks:=False, Transpose:=False

End Sub


' DB接続
Public Sub DBConnect(Optional ByVal ReadOnly As Integer = 1)
    Set objCon = New ADODB.Connection
    ' ReadOnly=1 読み込みのみ
    objCon.Open "Driver={Microsoft Excel Driver (*.xls, *.xlsx, *.xlsm, *.xlsb)};" & _
                "DBQ=" & ThisWorkbook.FullName & ";" & _
                "ReadOnly=" & ReadOnly
    objCon.CursorLocation = adUseClient

End Sub


' オブジェクトの解放
Public Sub objectRelease()
    
    If Not (objRS Is Nothing) Then
        objRS.Close
        Set objRS = Nothing
    End If
       
    If Not (objCon Is Nothing) Then
        objCon.Close
        Set objCon = Nothing
    End If
    
End Sub

Private Sub 固定枠とフィルタ()
    
    Sheets(strOutputSheetName).Select
    ' 固定枠
    Range("A1").Select
    With ActiveWindow
        .SplitColumn = 0
        .SplitRow = 1
    End With
    ActiveWindow.FreezePanes = True
    ' フィルタ
    If ActiveSheet.AutoFilterMode = False Then
        ActiveSheet.UsedRange.AutoFilter
    End If

    ' 列の幅の自動調整
    Cells.EntireColumn.AutoFit

End Sub


''''''''''''''''''''''' Reuse(未使用含む) '''''''''''''''''''''''

Private Sub TargetSheetSelect(ByVal varInSheet As Variant)
    If VarType(varInSheet) = vbString Then
        Sheets(varInSheet).Select
    Else
        varInSheet.Parent.Activate
        varInSheet.Select
    End If
End Sub

Private Sub 同名のワークシートを再表示(ByVal strOutSheetName As String)
    Dim i As Worksheet
    For Each i In Worksheets
        If (UCase(i.Name) = UCase(strOutSheetName)) Then
            Sheets(strOutSheetName).Visible = xlSheetVisible
        End If
    Next
End Sub


Private Sub 同名のワークシートをクリア(ByVal strOutSheetName As String)
    Dim i As Worksheet
    For Each i In Worksheets
        If (UCase(i.Name) = UCase(strOutSheetName)) Then
            Sheets(strOutSheetName).Select
            Cells.Select
            Selection.Clear
            Exit For
        End If
    Next
End Sub

Private Sub 同名のワークシートが無ければ追加(ByVal strOutSheetName As String)
    Dim i As Worksheet
    For Each i In Worksheets
        If (UCase(i.Name) = UCase(strOutSheetName)) Then
            Sheets(strOutSheetName).Select
            Exit Sub
        End If
    Next
    Sheets.Add Before:=Sheets(Sheets(Sheets.Count).Name)
    ActiveSheet.Name = strOutSheetName
End Sub

Private Sub 同名のワークシートを削除(ByVal strOutSheetName As String)
    Dim i As Worksheet
    For Each i In Worksheets
        If (UCase(i.Name) = UCase(strOutSheetName)) Then
            Application.DisplayAlerts = False
            i.Delete
            Application.DisplayAlerts = True
            Exit For
        End If
    Next
End Sub

Private Sub 同名のワークシートを非表示(ByVal strOutSheetName As String)
    Dim i As Worksheet
    For Each i In Worksheets
        If (UCase(i.Name) = UCase(strOutSheetName)) Then
            i.Visible = False
            Exit For
        End If
    Next
End Sub

Private Sub 選択セルの初期化()
    Dim i As Worksheet
    For Each i In Worksheets
        If (i.Visible = xlSheetVisible) Then
            i.Select
            ActiveSheet.Cells(1, 1).Select
        End If
    Next
End Sub

Public Sub CreateSheet(ByVal strOutSheetName As String)
    同名のワークシートを再表示 strOutSheetName
    同名のワークシートを削除 strOutSheetName

    DoEvents
    同名のワークシートが無ければ追加 strOutSheetName

    DoEvents
    Sheets(strOutSheetName).Select
End Sub

Private Sub SheetCopy(ByVal strInSheetName As String, _
                      ByVal strOutSheetName As String)
    
    '同名のワークシートを再表示 strOutSheetName
    '同名のワークシートをクリア strOutSheetName
    同名のワークシートを削除 strOutSheetName
    同名のワークシートが無ければ追加 strOutSheetName
    
    
    Sheets(strInSheetName).Range("A1").CurrentRegion.Copy
    'Sheets(strOutSheetName).Range("A1").PasteSpecial xlPasteValues
    Sheets(strOutSheetName).Range("A1").PasteSpecial xlPasteAll

        
End Sub

Private Function GetRegExpMatch(ByVal strInput As String, _
                        ByVal strFind As String, _
                        Optional ByVal booIgnoreCase As Boolean = True) As String
    Dim RE As Variant
    Set RE = CreateObject("VBScript.RegExp")
    With RE
        .Pattern = strFind
        .IgnoreCase = booIgnoreCase
        .Global = True
    End With

    Dim matches As Variant
    Set matches = RE.Execute(strInput)

    If matches.Count > 0 Then
        GetRegExpMatch = matches(0).Value
    End If

End Function

Private Function GetRegExpMatches(ByVal strInput As String, _
                        ByVal strFind As String, _
                        Optional ByVal booIgnoreCase As Boolean = True) As Object
    Dim RE As Variant
    Set RE = CreateObject("VBScript.RegExp")
    With RE
        .Pattern = strFind
        .IgnoreCase = booIgnoreCase
        .Global = True
    End With

    Set GetRegExpMatches = RE.Execute(strInput)

End Function

Private Function RegExpReplace(ByVal strInput As String, _
                       ByVal strFind As String, _
                       ByVal strReplace As String, _
                       Optional ByVal booIgnoreCase = True) As String
    Dim RE As Variant
    Set RE = CreateObject("VBScript.RegExp")
    With RE
        .Pattern = strFind
        .IgnoreCase = booIgnoreCase
        .Global = True
    End With

    Dim strResult As String
    strResult = RE.Replace(strInput, strReplace)
    
    RegExpReplace = strResult

End Function

Private Function GetUsedRangeEndRow() As Long
    GetUsedRangeEndRow = Cells(1, 1).SpecialCells(xlCellTypeLastCell).Row
End Function

Private Function GetUsedRangeEndColumn() As Long
    GetUsedRangeEndColumn = Cells(1, 1).SpecialCells(xlCellTypeLastCell).Column
End Function

Private Function GetTargetColumnStartRow(ByVal intColumn As Long) As Long
    Dim ret As Long
    On Error GoTo overflow
    If (Cells(1, intColumn).Value = "") Then
        ret = Cells(1, intColumn).End(xlDown).Row
    Else
        ret = 1
    End If
    GetTargetColumnStartRow = ret
    Exit Function
overflow:
    GetTargetColumnStartRow = -1
End Function

Private Function GetTargetColumnEndRow(ByVal intColumn As Long, Optional ByVal intRow As Long = 1) As Long
    Dim ret As Integer
    On Error GoTo overflow
    ' 空白時は、2回実行、オーバーフロー時は1
    ' integerの範囲を超えたらオーバーフローとする
    If (Cells(intRow, intColumn).Value = "") Then
        ret = Cells(intRow, intColumn).End(xlDown).Row
        ret = Cells(ret, intColumn).End(xlDown).Row
    Else
        ret = Cells(intRow, intColumn).End(xlDown).Row
    End If
    GetTargetColumnEndRow = ret
    Exit Function
overflow:
    GetTargetColumnEndRow = intRow
End Function



