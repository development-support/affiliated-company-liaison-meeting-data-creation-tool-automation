Attribute VB_Name = "MakeS4_Sub"
' 参照設定
'
' Microsoft ActiveX Data Objects 6.1 Library
' Microsoft Scripting Runtime
' Windows Script Host Object Model
'
Option Explicit

' DB Access関連定義
Public objDBCon As ADODB.Connection
Public objDBRec As ADODB.Recordset


' 指定シートの旧データ(６行目〜最終行)を削除するFunction
' [引数]
'   strSheetName As String：データがあるシート名
'
' [戻り値]
'   なし
Public Function DelDataForS4(ByVal strSheetName As String)
  Dim OrgWs As Worksheet
  Dim ObjWs As Worksheet
  Dim rowLast As Long

  Debug.Print "(DelDataForS4):strSheetName = " & strSheetName

  Set OrgWs = ActiveSheet
  Set ObjWs = Worksheets(strSheetName)

  ' シート切り替え
  ObjWs.Activate

  ' C列の最終行を求める
  rowLast = Cells(Rows.Count, 3).End(xlUp).Row
  Debug.Print "(DelDataForS4):rowLast = " & rowLast

  ' ６行目〜最終行までを削除する
  Range("6:" & rowLast).Delete

  ' ヘッダ部分をテンプレートより上書きコピーする
  Range("S4_xQRF_HEADER").Copy Destination:=ObjWs.Range("A1")

  ' 元のシートに切替
  OrgWs.Activate

End Function

' 前Quarterのデータ(Previous data)、当該Quarterのデータ(Recent data)
' を基に差分データを作成(シート出力)するFunction
' [引数]
'   strCurDataSheet As String：今期データのシート名
'   strPreDataSheet As String：前期データのシート名
'   strOutputSheet As String ：出力シート名
' [戻り値]
'   なし
Public Function MakeS4DiffData(ByVal strCurDataSheet As String, _
                               ByVal strPreDataSheet As String, _
                               ByVal strOutputSheet As String)

  Debug.Print "(MakeS4DiffData):strCurDataSheet = " & strCurDataSheet & _
              " strPreDataSheet = " & strPreDataSheet & _
              " strOutputSheet = " & strOutputSheet

  ' 出力シート作成
  Call MakeSheet(strOutputSheet)

  ' DB(Sheet)接続
  Call ConnectDB

  ' 前期/今期のデータを比較し、差分を抽出
  Call MakeDiffResult(strCurDataSheet, strPreDataSheet, strOutputSheet)

  ' DB(Sheet)切断
  Call ReleaseDB

  ' 追加/削除ラベルを付与した見出し列を追加(order_name->order_name2)
  Call AddHeadingWithLabel(strOutputSheet)

  ' 枠の固定とオートフィルタ等を設定
  Call SetStyleOfSheet(strOutputSheet)

  ' 3列目の表示書式指定文字を設定
  Columns(3).NumberFormatLocal = "0"

End Function

' 該当BookをDataBaseとして読込むFunction
' [引数]
'   なし
' [戻り値]
'   なし
Public Function ConnectDB()
  Set objDBCon = New ADODB.Connection

  ' ReadOnly=1 読み込みのみ
  objDBCon.Open "Driver={Microsoft Excel Driver (*.xls, *.xlsx, *.xlsm, *.xlsb)};" & _
                "DBQ=" & ThisWorkbook.FullName & ";" & _
                "ReadOnly=1"
  objDBCon.CursorLocation = adUseClient

End Function

' 該当BookをDataBaseとして読込んだ状態を解除するFunction
' [引数]
'   なし
' [戻り値]
'   なし
Public Function ReleaseDB()

  If Not (objDBRec Is Nothing) Then
    objDBRec.Close
    Set objDBRec = Nothing
  End If

  If Not (objDBCon Is Nothing) Then
    objDBCon.Close
    Set objDBCon = Nothing
  End If

End Function

' 前Quarterのデータ(Previous data)、当該Quarterのデータ(Recent data)
' を比較し、差分抽出結果をシート出力するFunction
' [引数]
'   strCurDataSheet As String：今期データのシート名
'   strPreDataSheet As String：前期データのシート名
'   strOutputSheet As String ：出力シート名
' [戻り値]
'   なし
Private Function MakeDiffResult(ByVal strCurDataSheet As String, _
                                ByVal strPreDataSheet As String, _
                                ByVal strOutputSheet As String)
  Dim i As Long
  Dim strSQLDiff As String
  Dim strSQLAdd As String
  Dim strSQLDel As String
  Dim strSQLJoin As String

  Debug.Print "(MakeDiffResult):strCurDataSheet = " & strCurDataSheet & _
              " strPreDataSheet = " & strPreDataSheet & _
              " strOutputSheet = " & strOutputSheet

  ' 差分
  strSQLDiff = "SELECT IIf((旧.[(百万)] = 新.[(百万)]),'○','×') as 差分, " & _
    "新.office_name, " & _
    "新.head_code, " & _
    "新.business_code, " & _
    "Format(新.charge_post_code,'0000') as charge_post_code, " & _
    "新.charge_post_name, " & _
    "新.cost_no, " & _
    "新.order_name, " & _
    "新.technology_division_name, " & _
    "新.job_informal_jp, " & _
    "新.m_index, " & _
    "旧.[sum(acceptance_cost)] as [Last_sum(acceptance_cost)], " & _
    "旧.[(百万)] as [Last_(百万)], " & _
    "新.[sum(acceptance_cost)] as [New_sum(acceptance_cost)], " & _
    "新.[(百万)] as [New_(百万)], " & _
    "新.[sum(acceptance_cost)]-旧.[sum(acceptance_cost)] as [差分_sum(acceptance_cost)], " & _
    "新.[(百万)]-旧.[(百万)] as [差分_(百万)] " & _
    "FROM [" & strPreDataSheet & "$] as 旧 INNER JOIN [" & strCurDataSheet & "$] as 新 ON " & _
    "(旧.office_name = 新.office_name) AND " & _
    "(旧.technology_division_name = 新.technology_division_name) AND " & _
    "(旧.m_index = 新.m_index) AND " & _
    "(TRIM(旧.order_name) = TRIM(新.order_name))"

  ' 追加
  strSQLAdd = "SELECT IIf([旧].[office_name] Is Null,'追加','') AS 差分, " & _
    "新.office_name, " & _
    "新.head_code, " & _
    "新.business_code, " & _
    "Format(新.charge_post_code,'0000') as charge_post_code, " & _
    "新.charge_post_name, " & _
    "新.cost_no, " & _
    "新.order_name, " & _
    "新.technology_division_name, " & _
    "新.job_informal_jp, " & _
    "新.m_index, " & _
    "0 as [Last_sum(acceptance_cost)], " & _
    "0 as [Last_(百万)], " & _
    "新.[sum(acceptance_cost)] as [New_sum(acceptance_cost)], " & _
    "新.[(百万)] as [New_(百万)], " & _
    "新.[sum(acceptance_cost)] as [差分_sum(acceptance_cost)], " & _
    "新.[(百万)] as [差分_(百万)] " & _
    "FROM [" & strCurDataSheet & "$] as 新 LEFT JOIN [" & strPreDataSheet & "$] as 旧 ON " & _
    "(旧.office_name = 新.office_name) AND " & _
    "(旧.technology_division_name = 新.technology_division_name) AND " & _
    "(旧.m_index = 新.m_index) AND " & _
    "(TRIM(旧.order_name) = TRIM(新.order_name)) " & _
    "WHERE (((旧.office_name) Is Null))"

  ' 削除
  strSQLDel = "SELECT IIf([新].[office_name] Is Null,'削除','') AS 差分, " & _
    "旧.office_name, " & _
    "旧.head_code, " & _
    "旧.business_code, " & _
    "Format(旧.charge_post_code,'0000') as charge_post_code, " & _
    "旧.charge_post_name, " & _
    "旧.cost_no, " & _
    "旧.order_name, " & _
    "旧.technology_division_name, " & _
    "旧.job_informal_jp, " & _
    "旧.m_index, " & _
    "旧.[sum(acceptance_cost)] as [Last_sum(acceptance_cost)], " & _
    "旧.[(百万)] as [Last_(百万)], " & _
    "0 as [New_sum(acceptance_cost)], " & _
    "0 as [New_(百万)], " & _
    "0-旧.[sum(acceptance_cost)] as [差分_sum(acceptance_cost)], " & _
    "0-旧.[(百万)] as [差分_(百万)] " & _
    "FROM [" & strPreDataSheet & "$] as 旧 LEFT JOIN [" & strCurDataSheet & "$] as 新 ON " & _
    "(旧.office_name = 新.office_name) AND " & _
    "(旧.technology_division_name = 新.technology_division_name) AND " & _
    "(旧.m_index = 新.m_index) AND " & _
    "(TRIM(旧.order_name) = TRIM(新.order_name)) " & _
    "WHERE (((新.office_name) Is Null))"

  ' 結合  = 差分(差分/追加/削除)リストの作成
  strSQLJoin = "select * from (" & strSQLDiff & ") " & _
               "union select * from (" & strSQLAdd & ") " & _
               "union select * from (" & strSQLDel & ") " & _
               "order by office_name, job_informal_jp, charge_post_code, business_code, cost_no, order_name, m_index"

  Set objDBRec = objDBCon.Execute(strSQLJoin)

  ' 作成した差分リストをシートに出力
  ' 列の見出し
  For i = 0 To objDBRec.Fields.Count - 1
    Sheets(strOutputSheet).Range("A1").Offset(0, i).Value = objDBRec.Fields(i).Name
  Next
  ' データ
  Sheets(strOutputSheet).Range("A2").CopyFromRecordset objDBRec

End Function

' 差分抽出結果において、order_nameに追加/削除ラベルを付与した、
' order_name2を作成するFunction
' [引数]
'   strOutputSheet As String ：出力シート名
' [戻り値]
'   なし
Private Function AddHeadingWithLabel(ByVal strOutputSheet As String)
  Dim rngCur As Range
  Dim rngColumn As Range
  Dim rngData As Range
  Dim colTarget As Long

  Debug.Print "(AddHeadingWithLabel):strOutputSheet = " & strOutputSheet
  colTarget = 8

  ' 1列追加
  Sheets(strOutputSheet).Columns(colTarget).Insert shift:=xlToRight, _
                                                   CopyOrigin:=xlFormatFromLeftOrAbove
  ' 列の見出し付与
  Sheets(strOutputSheet).Cells(1, colTarget).Value = "order_name2"

  ' 書き込み対象領域を取得
  ' 表の上端でCurrentRegion
  Set rngCur = Sheets(strOutputSheet).Range("A1").CurrentRegion
  ' 列の見出しの範囲をxldown
  Set rngColumn = Sheets(strOutputSheet).Columns(colTarget)
  ' 重なる領域を取得
  Set rngData = Intersect(rngCur, rngColumn)

  ' 対象領域に対して、(追加)、(削除)のラベルを付与する数式を入力
  ' 数式入力
  rngData(2, 1).Formula = "=IF(OR(A2=""追加"",A2=""削除""),""(""&A2&"")""&I2,I2)"
  ' コピー
  rngData(2, 1).Copy
  ' 貼り付け
  Set rngData = rngData.Offset(2, 0).Resize(rngData.Rows.Count - 2, 1)
  rngData.PasteSpecial Paste:=xlPasteFormulas, Operation:=xlNone, _
    SkipBlanks:=False, Transpose:=False

End Function

' 指定シートに、枠の固定とオートフィルタを設定し、列幅を自動調整するFunction
' [引数]
'   strOutputSheet As String ：出力シート名
' [戻り値]
'   なし
Private Function SetStyleOfSheet(ByVal strOutputSheet As String)

  Debug.Print "(SetStyleOfSheet):strOutputSheet = " & strOutputSheet

  Sheets(strOutputSheet).Select

  ' 枠の固定を設定
  Range("A1").Select
  With ActiveWindow
    .SplitColumn = 0
    .SplitRow = 1
  End With
  ActiveWindow.FreezePanes = True

  ' オートフィルタを設定
  If ActiveSheet.AutoFilterMode = False Then
    ActiveSheet.UsedRange.AutoFilter
  End If

  ' 列の幅の自動調整
  Cells.EntireColumn.AutoFit

End Function

' 指定シートのデータを基にPivot Tableを作成するFunction
' [引数]
'   strSheetName As String：Pivot Tableを出力するシート名
'   strTableName As String：Pivot Tableの作成基となるテーブル名
'
' [戻り値]
'   Boolean：True  (処理成功)
'          ：False (処理失敗)
Public Function MakeS4PivotTable(ByVal strSheetName As String, _
                                 ByVal strTableName As String, _
                                 ByVal QuarterNum As Long) As Boolean
  Dim itm As PivotItem

  Debug.Print "(MakeS4PivotTable):strSheetName = " & strSheetName & _
              " strTableName = " & strTableName

  On Error GoTo 0
  On Error GoTo MAKE_S4_PIVOT_TABLE_ERROR

  ' ピボットテーブル作成
  ActiveWorkbook.PivotCaches.Create(xlDatabase, _
    strTableName).CreatePivotTable Sheets.Add.Range("A4")

  ' シート名を指定のものに変更
  ActiveSheet.Name = strSheetName

  ' フィールドのレイアウトを設定
  With ActiveSheet.PivotTables(1)
    ' 列フィールド
    .PivotFields("job_informal_jp").Orientation = xlRowField
    .PivotFields("order_name2").Orientation = xlRowField
    ' 行フィールド
    .PivotFields("m_index").Orientation = xlColumnField
    ' データ(Σ値)フィールド
    .PivotFields("Last_(百万)").Orientation = xlDataField
    .PivotFields("New_(百万)").Orientation = xlDataField
    .PivotFields("差分_(百万)").Orientation = xlDataField
    ' フィルターフィールド
    .PivotFields("office_name").Orientation = xlPageField
    .PivotFields("technology_division_name").Orientation = xlPageField

    ' 列の総計のみ表示させる
    .ColumnGrand = True
    .RowGrand = False
  End With

  ' m_indexフィールドのフィルターをQuarterに応じて設定
  For Each itm In ActiveSheet.PivotTables(1).PivotFields("m_index").PivotItems
    Select Case itm.Value
      Case 1:
        If (QuarterNum = 1) Then
          itm.Visible = True
        Else
          itm.Visible = False
        End If
      Case 4:
        If (QuarterNum = 2) Then
          itm.Visible = True
        Else
          itm.Visible = False
        End If
      Case 7:
        If (QuarterNum = 3) Then
          itm.Visible = True
        Else
          itm.Visible = False
        End If
      Case 10:
        If (QuarterNum = 4) Then
          itm.Visible = True
        Else
          itm.Visible = False
        End If
    End Select
  Next itm

  ' job_informal_jpフィールドのレイアウト設定を変更(コンパクト形式を解除)
  ActiveSheet.PivotTables(1).PivotFields("job_informal_jp").LayoutCompactRow = False

  MakeS4PivotTable = True
  Exit Function

MAKE_S4_PIVOT_TABLE_ERROR:
  MsgBox "テーブル(" & strTableName & _
         ")を基にしたピボットテーブル作成が失敗しました。" & vbCrLf & _
         "Err.Source = " & Err.Source & vbCrLf & _
         "Err.Number = " & Err.Number & vbCrLf & _
         "Err.Description = " & Err.Description
  MakeS4PivotTable = False

End Function

' スライド４用出力シートにおいて、今期Quarterデータのテンプレートを
' 作成するFunction
' [引数]
'   strS4Sheet As String   ：スライド４用出力シート名
'   strPivotSheet As String：スライド４用Pivot Tableを作成したシート名
'
' [戻り値]
'   なし
Public Function MakeS4Template(ByVal strS4Sheet As String, _
                               ByVal strPivotSheet As String)
  Dim OrgWs As Worksheet
  Dim S4Ws As Worksheet
  Dim PivotWs As Worksheet
  Dim maxOfficeNum As Long
  Dim maxJobInfoNum As Long
  Dim i As Long
  Dim j As Long
  Dim k As Long
  Dim rowCur As Long
  Dim colCur As Long

  Debug.Print "(MakeS4Template):strS4Sheet = " & strS4Sheet & _
              " strPivotSheet = " & strPivotSheet

  Set OrgWs = ActiveSheet
  Set S4Ws = Worksheets(strS4Sheet)
  Set PivotWs = Worksheets(strPivotSheet)

  ' Pivotテーブルのシートに切替
  PivotWs.Activate

  ' Pivotテーブルより必要情報を収集
  With ActiveSheet.PivotTables(1)
    ' Pivotテーブルより、全関係会社名(office_name)を取得
    maxOfficeNum = .PivotFields("office_name").PivotItems.Count
    ReDim gOfficeList(maxOfficeNum)
    ReDim gOfficeInfoS4(maxOfficeNum)

    For i = 1 To maxOfficeNum
      gOfficeList(i) = .PivotFields("office_name").PivotItems(i)
      gOfficeInfoS4(i).OfficeName = .PivotFields("office_name").PivotItems(i)
      Debug.Print "gOfficeList(" & i & ") = " & gOfficeList(i) & vbCrLf
    Next i

    ' Pivotテーブルより、全部署名(job_informal_jp)を取得
    maxJobInfoNum = .PivotFields("job_informal_jp").PivotItems.Count
    ReDim gJobInfoList(maxJobInfoNum)

    ' 全関係会社の関連情報において、コスト別の部署情報を初期化
    For i = 1 To maxOfficeNum
      ReDim gOfficeInfoS4(i).PaidFreeJobInfo(maxJobInfoNum)
      ReDim gOfficeInfoS4(i).PaidJobInfo(maxJobInfoNum)
      ReDim gOfficeInfoS4(i).FreeJobInfo(maxJobInfoNum)
    Next i

    For j = 1 To maxJobInfoNum
      gJobInfoList(j) = .PivotFields("job_informal_jp").PivotItems(j)
      Debug.Print "gJobInfoList(" & j & ") = " & gJobInfoList(j) & vbCrLf
    Next j
  End With

  ' スライド４用出力シートに切替
  S4Ws.Activate

  ' "A1"セルを今期Quarterに書き換える
  S4Ws.Range("A1").Value = gSettingList.Quarter & "QRF"

  rowCur = S4Ws.Range("A1").Row + 5
  colCur = S4Ws.Range("A1").Column

  For i = 1 To maxOfficeNum
    ' 関係会社名を記載
    S4Ws.Cells(rowCur, colCur).Value = gOfficeInfoS4(i).OfficeName
    Set gOfficeInfoS4(i).RngOffice = _
            S4Ws.Range(S4Ws.Cells(rowCur, colCur).Address(False, False))
    rowCur = rowCur + 1
    colCur = colCur + 1
    ' 有償/無償のひな形をTempleteシートからコピー
    Range("S4_有償_無償").Copy Destination:=S4Ws.Cells(rowCur, colCur)
    Set gOfficeInfoS4(i).RngPaidFree = _
            S4Ws.Range(S4Ws.Cells(rowCur, colCur).Address(False, False))
    rowCur = rowCur + 4
    colCur = colCur + 1
    ' 全部署名を記載
    For j = 1 To maxJobInfoNum
      S4Ws.Cells(rowCur, colCur).Value = gJobInfoList(j)
      S4Ws.Cells(rowCur, colCur).Font.Bold = True
      Set gOfficeInfoS4(i).PaidFreeJobInfo(j) = _
              S4Ws.Range(S4Ws.Cells(rowCur, colCur).Address(False, False))

      For k = 1 To 4
        S4Ws.Cells(rowCur, colCur + k).Font.Bold = True

        Select Case (k)
          Case 1
            ' "小計"を記載
            S4Ws.Cells(rowCur, colCur + k).Value = "小計"
          Case Else
            ' デフォルト値 = 0を設定
            S4Ws.Cells(rowCur, colCur + k).Value = 0
        End Select
      Next k

      rowCur = rowCur + 1
    Next j

    ' 総計のひな形をTempleteシートからコピー
    Range("S4_総計").Copy Destination:=S4Ws.Cells(rowCur, colCur)
    Set gOfficeInfoS4(i).PaidFreeTotal = S4Ws.Cells(rowCur, colCur)
    For k = 1 To 4
      Select Case (k)
        Case 2, 3, 4
          ' デフォルト値 = 0を設定
          S4Ws.Cells(rowCur, colCur + k).Value = 0
        Case Else
          ' NOP
      End Select
    Next k

    rowCur = rowCur + 3
    colCur = colCur - 1

    ' 有償のひな形をTempleteシートからコピー
    Range("S4_有償").Copy Destination:=S4Ws.Cells(rowCur, colCur)
    Set gOfficeInfoS4(i).RngPaid = _
            S4Ws.Range(S4Ws.Cells(rowCur, colCur).Address(False, False))
    rowCur = rowCur + 4
    colCur = colCur + 1
    ' 全部署名を記載
    For j = 1 To maxJobInfoNum
      S4Ws.Cells(rowCur, colCur).Value = gJobInfoList(j)
      S4Ws.Cells(rowCur, colCur).Font.Bold = True
      Set gOfficeInfoS4(i).PaidJobInfo(j) = _
              S4Ws.Range(S4Ws.Cells(rowCur, colCur).Address(False, False))

      For k = 1 To 4
        S4Ws.Cells(rowCur, colCur + k).Font.Bold = True

        Select Case (k)
          Case 1
            ' "小計"を記載
            S4Ws.Cells(rowCur, colCur + k).Value = "小計"
          Case Else
            ' デフォルト値 = 0を設定
            S4Ws.Cells(rowCur, colCur + k).Value = 0
        End Select
      Next k

      rowCur = rowCur + 1
    Next j

    ' 総計のひな形をTempleteシートからコピー
    Range("S4_総計").Copy Destination:=S4Ws.Cells(rowCur, colCur)
    Set gOfficeInfoS4(i).PaidTotal = S4Ws.Cells(rowCur, colCur)
    For k = 1 To 4
      Select Case (k)
        Case 2, 3, 4
          ' デフォルト値 = 0を設定
          S4Ws.Cells(rowCur, colCur + k).Value = 0
        Case Else
          ' NOP
      End Select
    Next k

    rowCur = rowCur + 3
    colCur = colCur - 1

    ' 無償のひな形をTempleteシートからコピー
    Range("S4_無償").Copy Destination:=S4Ws.Cells(rowCur, colCur)
    Set gOfficeInfoS4(i).RngFree = _
            S4Ws.Range(S4Ws.Cells(rowCur, colCur).Address(False, False))
    rowCur = rowCur + 4
    colCur = colCur + 1
    ' 全部署名を記載
    For j = 1 To maxJobInfoNum
      S4Ws.Cells(rowCur, colCur).Value = gJobInfoList(j)
      S4Ws.Cells(rowCur, colCur).Font.Bold = True
      Set gOfficeInfoS4(i).FreeJobInfo(j) = _
              S4Ws.Range(S4Ws.Cells(rowCur, colCur).Address(False, False))

      For k = 1 To 4
        S4Ws.Cells(rowCur, colCur + k).Font.Bold = True

        Select Case (k)
          Case 1
            ' "小計"を記載
            S4Ws.Cells(rowCur, colCur + k).Value = "小計"
          Case Else
            ' デフォルト値 = 0を設定
            S4Ws.Cells(rowCur, colCur + k).Value = 0
        End Select
      Next k

      rowCur = rowCur + 1
    Next j

    ' 総計のひな形をTempleteシートからコピー
    Range("S4_総計").Copy Destination:=S4Ws.Cells(rowCur, colCur)
    Set gOfficeInfoS4(i).FreeTotal = S4Ws.Cells(rowCur, colCur)
    For k = 1 To 4
      Select Case (k)
        Case 2, 3, 4
          ' デフォルト値 = 0を設定
          S4Ws.Cells(rowCur, colCur + k).Value = 0
        Case Else
          ' NOP
      End Select
    Next k

    rowCur = rowCur + 2
    colCur = colCur - 2
  Next i

  ' 元のシートに切替
  OrgWs.Activate

End Function

' スライド４用Pivot Tableを操作し、各関係会社毎に３種類のデータをスライド４用出力
' シートに反映するFunction
' [引数]
'   strS4Sheet As String   ：スライド４用出力シート名
'   strPivotSheet As String：スライド４用Pivot Tableを作成したシート名
'
' [戻り値]
'   なし
Public Function CopyS4DataFromPivot(ByVal strS4Sheet As String, _
                                    ByVal strPivotSheet As String)
  Const strCostList As String = "有償/無償,有償,無償"
  Const ProgPercentOfThisProc As Long = 20  ' 本処理の全体処理に占める進捗率
  Const ProgRateUpToThisProc As Long = 80  ' 本処理までの全体進捗率
  Dim vCostList As Variant
  Dim vCostItem As Variant
  Dim OrgWs As Worksheet
  Dim S4Ws As Worksheet
  Dim PivotWs As Worksheet
  Dim i As Long
  Dim BaseProgress As Long
  Dim NowProgress As Long

  Debug.Print "(CopyS4DataFromPivot):strS4Sheet = " & strS4Sheet & _
              " strPivotSheet = " & strPivotSheet

  Set OrgWs = ActiveSheet
  Set S4Ws = Worksheets(strS4Sheet)
  Set PivotWs = Worksheets(strPivotSheet)
  vCostList = Split(strCostList, ",")
  BaseProgress = Int(ProgPercentOfThisProc / UBound(gOfficeList))

  ' Pivotテーブルのシートに切替
  PivotWs.Activate

  ' 関係会社分、繰り返し
  For i = 1 To UBound(gOfficeList)
    NowProgress = ProgRateUpToThisProc + (BaseProgress * i)
    Call UpdateProgressBar(NowProgress)

    ' 費用項目(有償/無償など)分、繰り返し
    For Each vCostItem In vCostList
      ' Pivot Tableのフィルター設定を操作
      Call OperateS4PivotTable(i, vCostItem, strPivotSheet)

      ' データが存在する部署のみ、スライド４用出力シートにコピー
      Call CopyPivotJobInfoToS4(i, vCostItem, strS4Sheet, strPivotSheet)

      ' 関係会社の関連情報リスト(スライド４用)を更新
      Call UpdateS4Info(strS4Sheet)
    Next vCostItem
  Next i

  ' 元のシートに切替
  OrgWs.Activate

End Function

' PivotTableのフィルタ設定を、指定された関係会社、費用項目に応じて
' 操作するFunction
' [引数]
'   office_idx As Long     ：関係会社(office_name)のindex番号
'   cost_itm As String     ：費用項目(有償/無償、有償、無償)
'   strPivotSheet As String：スライド４用Pivot Tableを作成したシート名
'
' [戻り値]
'   なし
Public Function OperateS4PivotTable(ByVal office_idx As Long, _
                                    ByVal cost_itm As String, _
                                    ByVal strPivotSheet As String)
  Dim OrgWs As Worksheet
  Dim PivotWs As Worksheet
  Dim itm As PivotItem

  Debug.Print "(OperateS4PivotTable):office_idx = " & office_idx & _
              " cost_itm = " & cost_itm & _
              " strPivotSheet = " & strPivotSheet

  Set OrgWs = ActiveSheet
  Set PivotWs = Worksheets(strPivotSheet)

  ' Pivotテーブルのシートに切替
  PivotWs.Activate

  With PivotWs.PivotTables(1).PivotFields("office_name")
    ' office_nameフィールドのフィルターを初期化(全て選択状態にする)
    For Each itm In .PivotItems
      itm.Visible = True
    Next itm

    ' office_nameフィールドのフィルターを設定
    For Each itm In .PivotItems
      Select Case itm.Value
        Case gOfficeInfoS4(office_idx).OfficeName
          itm.Visible = True
        Case Else
          itm.Visible = False
      End Select
    Next itm
  End With

  With PivotWs.PivotTables(1).PivotFields("technology_division_name")
    ' technology_division_nameフィールドのフィルターを設定
    Select Case cost_itm
      Case "有償/無償"
        For Each itm In .PivotItems
          itm.Visible = True
        Next itm
      Case "有償"
        For Each itm In .PivotItems
          Select Case itm.Value
            Case "フィールドサポート費", "有償ソフト(有償)"
              itm.Visible = True
            Case Else
              itm.Visible = False
          End Select
        Next itm
      Case "無償"
        For Each itm In .PivotItems
          Select Case itm.Value
            Case "技術研究費", "設計原価", "販売・技術支援費"
              itm.Visible = True
            Case Else
              itm.Visible = False
          End Select
        Next itm
    End Select
  End With

  ' 元のシートに切替
  OrgWs.Activate

End Function

' PivotTableにデータが存在する部署のみ、スライド４用出力シートに
' コピーするFunction
' [引数]
'   office_idx As Long     ：関係会社(office_name)のindex番号
'   cost_itm As String     ：費用項目(有償/無償、有償、無償)
'   strS4Sheet As String   ：スライド４用出力シート名
'   strPivotSheet As String：スライド４用Pivot Tableを作成したシート名
'
' [戻り値]
'   なし
Public Function CopyPivotJobInfoToS4(ByVal office_idx As Long, _
                                     ByVal cost_itm As String, _
                                     ByVal strS4Sheet As String, _
                                     ByVal strPivotSheet As String)
  Dim OrgWs As Worksheet
  Dim S4Ws As Worksheet
  Dim PivotWs As Worksheet
  Dim FoundCell As Range
  Dim rngTemp As Range
  Dim strCopySrcRng As String
  Dim strCopyDstRng As String
  Dim i As Long
  Dim j As Long
  Dim vStartRng As Variant
  Dim vEndRng As Variant
  Dim numData As Long
  Dim rowTemp As Long
  Dim colTemp As Long

  Debug.Print "(CopyPivotJobInfoToS4):office_idx = " & office_idx & _
              " cost_itm = " & cost_itm & _
              " strS4Sheet = " & strS4Sheet & _
              " strPivotSheet = " & strPivotSheet

  Set OrgWs = ActiveSheet
  Set S4Ws = Worksheets(strS4Sheet)
  Set PivotWs = Worksheets(strPivotSheet)

  ' データが存在する部署のみ、スライド４用出力シートにコピー
  For i = 1 To UBound(gJobInfoList)
    ' Pivotテーブルのシートに切替
    PivotWs.Activate

    Set FoundCell = Cells.Find(What:=gJobInfoList(i), LookAt:=xlWhole)
    If FoundCell Is Nothing Then
      ' 部署名が見つからなかった場合、SKIP
      GoTo SKIP_JOB_INFO_S4
    End If

    ' 該当部署名の今期Quarter分のデータをコピー
    ' (1)該当部署名より、コピー元範囲を特定する。
    With PivotWs.PivotTables(1).PivotFields("job_informal_jp")
      vStartRng = Split(.PivotItems(i).LabelRange.Address, ":")
      vEndRng = Split(.PivotItems(i).DataRange.Address, ":")
    End With

    strCopySrcRng = vStartRng(0) & ":" & vEndRng(1)
    Debug.Print "(CopyPivotJobInfoToS4):strCopySrcRng = " & strCopySrcRng

    numData = Val(FindNumberRegExp(vStartRng(1))) - Val(FindNumberRegExp(vStartRng(0)))
    Debug.Print "(CopyPivotJobInfoToS4):numData = " & numData

    ' (2)コピー先のセルを特定し、アイテム数分、行を挿入する。
    Select Case (cost_itm)
      Case "有償/無償"
        rowTemp = gOfficeInfoS4(office_idx).PaidFreeJobInfo(i).Row
        colTemp = gOfficeInfoS4(office_idx).PaidFreeJobInfo(i).Column
      Case "有償"
        rowTemp = gOfficeInfoS4(office_idx).PaidJobInfo(i).Row
        colTemp = gOfficeInfoS4(office_idx).PaidJobInfo(i).Column
      Case "無償"
        rowTemp = gOfficeInfoS4(office_idx).FreeJobInfo(i).Row
        colTemp = gOfficeInfoS4(office_idx).FreeJobInfo(i).Column
    End Select

    S4Ws.Rows(rowTemp & ":" & rowTemp + numData).Insert shift:=xlShiftDown
    S4Ws.Rows(rowTemp + numData + 1).Delete

    strCopyDstRng = S4Ws.Cells(rowTemp, colTemp).Address
    Range(strCopySrcRng).Copy Destination:=S4Ws.Range(strCopyDstRng)
SKIP_JOB_INFO_S4:
  Next i

  ' Pivotテーブルのシートに切替
  PivotWs.Activate

  ' 総計部分のデータをコピー
  ' (1)コピー元範囲を特定する。
  Set FoundCell = Cells.Find(What:="総計", LookAt:=xlWhole)
  If FoundCell Is Nothing Then
    ' 総計が見つからなかった場合、終了(通常有り得ない)
    Debug.Print "(CopyPivotJobInfoToS4):総計 not found." & _
                " office_idx = " & office_idx & _
                " cost_itm = " & cost_itm
    Exit Function
  End If

  strCopySrcRng = FoundCell.Address & ":" & _
                  Cells(FoundCell.Row, FoundCell.Column + 4).Address
  Debug.Print "(CopyPivotJobInfoToS4):strCopySrcRng = " & strCopySrcRng

  Select Case (cost_itm)
    Case "有償/無償"
      Set rngTemp = gOfficeInfoS4(office_idx).PaidFreeTotal
    Case "有償"
      Set rngTemp = gOfficeInfoS4(office_idx).PaidTotal
    Case "無償"
      Set rngTemp = gOfficeInfoS4(office_idx).FreeTotal
  End Select

  strCopyDstRng = rngTemp.Address
  Range(strCopySrcRng).Copy Destination:=S4Ws.Range(strCopyDstRng)

  ' 元のシートに切替
  OrgWs.Activate

End Function

' スライド４用出力シートにおいて、各関係会社の情報を更新するFunction
' [引数]
'   strS4Sheet As String   ：スライド４用出力シート名
'
' [戻り値]
'   なし
Public Function UpdateS4Info(ByVal strS4Sheet As String)
  Const strCostList As String = "有償/無償,有償,無償"
  Dim vCostList As Variant
  Dim OrgWs As Worksheet
  Dim S4Ws As Worksheet
  Dim Found1st As Range
  Dim Found2nd As Range
  Dim Found3rd As Range
  Dim Found4th As Range
  Dim i As Long
  Dim j As Long
  Dim k As Long

  Debug.Print "(UpdateS4Info):strS4Sheet = " & strS4Sheet

  Set OrgWs = ActiveSheet
  Set S4Ws = Worksheets(strS4Sheet)
  vCostList = Split(strCostList, ",")

  ' スライド４用出力シートに切替
  S4Ws.Activate

  ' 関係会社分、繰り返し
  For i = 1 To UBound(gOfficeList)
    Set Found1st = Columns(1).Find(What:=gOfficeList(i), LookAt:=xlWhole)
    ' 関係会社名を検索
    If Found1st Is Nothing Then
      ' 関係会社名が見つからなかった場合、SKIP
      GoTo SKIP_UPDATE_S4_INFO
    End If

    Set gOfficeInfoS4(i).RngOffice = Found1st

    ' コスト種別分、繰り返し
    For j = 0 To UBound(vCostList)
      ' コスト種別(有償/無償, 有償, 無償)を検索
      Set Found2nd = Columns(2).Find(What:=vCostList(j), _
                                     LookAt:=xlWhole, _
                                     After:=Cells(Found1st.Row, _
                                                  Found1st.Column + 1))
      If Found2nd Is Nothing Then
        ' コスト種別名が見つからなかった場合、SKIP
        GoTo SKIP_UPDATE_S4_INFO
      End If

      Select Case (j)
        Case 0
          Set gOfficeInfoS4(i).RngPaidFree = Found2nd
        Case 1
          Set gOfficeInfoS4(i).RngPaid = Found2nd
        Case 2
          Set gOfficeInfoS4(i).RngFree = Found2nd
      End Select

      ' 部署名分、繰り返し
      For k = 1 To UBound(gJobInfoList)
        ' 部署名を検索
        Set Found3rd = Columns(3).Find(What:=gJobInfoList(k), _
                                       LookAt:=xlWhole, _
                                       After:=Cells(Found2nd.Row, _
                                                    Found2nd.Column + 1))
        If Found3rd Is Nothing Then
          ' 部署名が見つからなかった場合、SKIP
          GoTo SKIP_UPDATE_S4_INFO
        End If

        Select Case (j)
          Case 0
            Set gOfficeInfoS4(i).PaidFreeJobInfo(k) = Found3rd
          Case 1
            Set gOfficeInfoS4(i).PaidJobInfo(k) = Found3rd
          Case 2
            Set gOfficeInfoS4(i).FreeJobInfo(k) = Found3rd
        End Select

        ' 部署名の右隣のセルに"小計"の記載が無い場合、再度記載する。
        If (Cells(Found3rd.Row, Found3rd.Column + 1) = "") Then
          Cells(Found3rd.Row, Found3rd.Column + 1) = "小計"
        End If
      Next k

      ' 総計を検索
      Set Found4th = Columns(3).Find(What:="総計", _
                                     LookAt:=xlWhole, _
                                     After:=Cells(Found2nd.Row, _
                                                  Found2nd.Column + 1))
      If Found4th Is Nothing Then
        ' 総計が見つからなかった場合、SKIP
        GoTo SKIP_UPDATE_S4_INFO
      End If

      Select Case (j)
        Case 0
          Set gOfficeInfoS4(i).PaidFreeTotal = Found4th
        Case 1
          Set gOfficeInfoS4(i).PaidTotal = Found4th
        Case 2
          Set gOfficeInfoS4(i).FreeTotal = Found4th
      End Select
    Next j
SKIP_UPDATE_S4_INFO:
  Next i

End Function
