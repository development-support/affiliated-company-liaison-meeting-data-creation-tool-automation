Attribute VB_Name = "ImportData_Sub"
Option Explicit

' ファイルを開くダイアログを表示し、選択したファイルを開くFunction
' ※処理成功した場合、引数にファイル名、Full Pathを返す
' [引数]
'   strFileName As String：選択したファイル名が返る
'   strFullPath As String：選択したファイルのFull Pathが返る
'
' [戻り値]
'   Boolean：True  (処理成功)
'          ：False (処理失敗)
Public Function FileOpenWithDialog(ByRef strFilename As String, _
                                   ByRef strFullPath As String) As Boolean
  Dim OpenFileName As String
  Dim wb As Workbook

  OpenFileName = Application.GetOpenFilename("Microsoft Excelブック,*.xls?")
  If OpenFileName <> "False" Then
    ' ファイル名のみを取得
    strFilename = Dir(OpenFileName)
    strFullPath = OpenFileName
    Debug.Print "(FileOpenWithDialog):strFilename = " & strFilename & vbCrLf & _
                "                     strFullPath = " & strFullPath
    '同名ブックが既に開かれているかチェック
    For Each wb In Workbooks
      If wb.Name = strFilename Then
        MsgBox strFilename & vbCrLf & _
               "は既に開いています", vbExclamation
        FileOpenWithDialog = False
        Exit Function
      End If
    Next wb
    ' ファイルを開く
    Workbooks.Open OpenFileName
  Else
    MsgBox "ファイル選択がキャンセルされました", vbExclamation
    FileOpenWithDialog = False
    Exit Function
  End If

  FileOpenWithDialog = True

End Function
